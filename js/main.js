
const DSSV = "DSSV";
var dssv = [];
// lấy dữ liệu lên từ localStorege
var dataJson = localStorage.getItem(DSSV);
if(dataJson){
    var dataRaw = JSON.parse(dataJson);
aw.map(function(item){
        return new SinhVien(
            item.ma,
            item.ten,
            item.email,
            item.matKhau,
            item.diemToan,
            item.diemLy,
            item.diemHoa,
        );
    })
    renderDssv(dssv)
}

function saveLocalStorage(){
    var dssvJson = JSON.stringify(dssv);

    localStorage.setItem(DSSV,dssvJson);
}

function themSinhVien() {
    var newSv = layThongTinTuForm();
    
    var isValid = true;

    // kiem tra ma sv
    isValid &= kiemTraRong(newSv.ma, "spanMaSV") && 
    kiemTraMaSv(newSv.ma,dssv, "spanMaSV"); 

    // kiểm tra tên 
    isValid &= kiemTraRong(newSv.ten, "spanTenSV"); 
    
    // kiểm tra email
    isValid &= kiemTraRong(newSv.email, "spanEmailSV") && 
    kiemTraEmail(newSv.email, "spanEmailSV");

    // kiểm tra Mật khẩu
    isValid &= kiemTraRong(newSv.matKhau, "spanMatKhau"); 

    // kiểm tra điểm
    isValid &= kiemTraRong(newSv.diemToan, "spanToan"); 
    isValid &= kiemTraRong(newSv.diemLy, "spanLy");
    isValid &= kiemTraRong(newSv.diemHoa, "spanHoa");





    if(isValid){
    saveLocalStorage();
    dssv.push(newSv);
    renderDssv(dssv);    
    resetForm();
}
}


function xoaSv(idSv){
    var index = dssv.findIndex(function(sv) {
        return sv.ma == idSv;
    });
    if (index == -1) {
        return;
    }

    dssv.splice(index, 1);
    console.log('dssv: ', dssv.length);
    renderDssv(dssv);   
    
}

function suaSv(idSv){
    var index = dssv.findIndex(function(sv) {
        return sv.ma == idSv;
    });
    if (index == -1) return;
    
    var sv = dssv[index];
    showThongTinLenForm(sv);
    document.getElementById("txtMaSV").disabled= true;
}

function capNhapSv(){
    var svEdit = layThongTinTuForm();
    console.log('svEdit: ', svEdit);
    var index = dssv.findIndex(function(sv) {
        return sv.ma == svEdit.ma;
    });
    if (index == -1) return;

    dssv[index] = svEdit;

    saveLocalStorage();
    renderDssv(dssv);
    resetForm();
    document.getElementById("txtMaSV").disabled= false;

}
