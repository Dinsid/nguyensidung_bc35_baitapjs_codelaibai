function layThongTinTuForm(){
    
    var maSv = document.getElementById("txtMaSV").value.trim();
    var tenSv = document.getElementById("txtTenSV").value.trim();
    var emailSv = document.getElementById("txtEmail").value.trim();
    var matKhau = document.getElementById("txtPass").value.trim();
    var diemToan = document.getElementById("txtDiemToan").value.trim();
    var diemLy = document.getElementById("txtDiemLy").value.trim();
    var diemHoa = document.getElementById("txtDiemHoa").value.trim();
    var sv =new SinhVien (maSv,tenSv,emailSv,matKhau,diemToan,diemLy,diemHoa);
    return sv;
}

function renderDssv(list){
    var contentHTML = "";

    for(var i=0;i<list.length;i++){
        var currentSv = list[i];

        var contentTr = `<tr>
        <td>${currentSv.ma}</td>
        <td>${currentSv.ten}</td>
        <td>${currentSv.email}</td>
        <td>${currentSv.tinhDTB()}</td>
        <td>
        <button onclick="xoaSv('${currentSv.ma}')" class = "btn btn-danger">Xóa</button>
        <button onclick="suaSv('${currentSv.ma}')" class = "btn btn-primary">Sửa</button>
        </td>
        </tr>`;

        contentHTML = contentHTML +contentTr;
    }
    document.getElementById("tbodySinhVien").innerHTML = contentHTML;
}

function showThongTinLenForm(sinhVien){
    document.getElementById("txtMaSV").value = sinhVien.ma;
    document.getElementById("txtTenSV").value = sinhVien.ten;
    document.getElementById("txtEmail").value = sinhVien.email;
    document.getElementById("txtPass").value = sinhVien.matKhau;
    document.getElementById("txtDiemToan").value = sinhVien.diemToan;
    document.getElementById("txtDiemLy").value = sinhVien.diemLy;
    document.getElementById("txtDiemHoa").value = sinhVien.diemHoa;
}

function resetForm(){
    document.getElementById("formQLSV").reset();
}